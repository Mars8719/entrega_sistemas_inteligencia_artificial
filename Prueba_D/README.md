# Actividad_1 
## Prueba Diagnostica


![Ejercicios de la prueba diagnostica](./imgs/Ejercicios.png)

## Desarrollo

### Primera Parte
![Punto 1](./imgs/punto_1.jpg)
![Punto 2-3](./imgs/punto_2-3.jpg)

### Segunda Parte

![Rock Paper Scissors Lizard Spock](./imgs/Rock%20Paper%20Scissors%20Lizard%20Spock.png)
![1D Bush Fire](./imgs/1D%20Bush%20Fire.png)
![Feature extraction](./imgs/Feature%20extraction.png)
